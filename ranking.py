SPACE = " "
def parse_line(line:str) -> list:
	rank = []
	name , * marks = line.strip().split(SPACE)
	marks = [int(_) for _ in marks]
	rank = [name] + marks
	return rank

def load_data() -> list:
	return [parse_line(line) for line in open('marks.txt')]
def arrange_rank(rank : list) -> str:
	ls = []
	for i in range(0,len(rank),2):

		for k in range(i + 1,len(rank)):
			ss = "g" 
			for j in range(1,4):
				if rank[i][j] > rank[k][j]:
					s = "g"
				elif j > 1 and j <= 5:
					ss = "c"
				else:
					ls.append(f"{rank[i][0]} < {rank[k][0]}")
					ss = "l"

			if ss != "c" and ss!= "l":
				ls.append(f"{rank[i][0]} > {rank[k][0]}")
			else:
				continue
			
	return ls
print(arrange_rank(load_data()))
