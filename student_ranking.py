class Student:
    def __init__(self, name, marks):
        self.name = name
        self.marks = marks

    def total_score(self):
        return sum(self.marks)

    def __lt__(self, other):
        if all(s > o for s, o in zip(self.marks, other.marks)):
            return False
        if all(o > s for s, o in zip(self.marks, other.marks)):
            return True
        return self.total_score() < other.total_score()

def rank_students(students):
    students.sort(reverse=True)
    return students

def read_students_from_file(file_path):
    students = []
    with open(file_path, 'r') as file:
        for line in file:
            parts = line.split()
            if len(parts) < 2:  # Ensure there's at least a name and one mark
                continue
            name = parts[0]
            marks = list(map(int, parts[1:]))
            students.append(Student(name, marks))
    return students

def get_ranked_students(file_path):
    ranked_students = rank_students(read_students_from_file(file_path))
    return [[student.name, student.marks] for i, student in enumerate(ranked_students)]

file_path = 'Student_Ranking.txt'
ranked_students = get_ranked_students(file_path)

print(' > '.join(student[0] for student in ranked_students))
